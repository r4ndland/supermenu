#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
 
float cantidad = 41;
 
sem_t A;
sem_t B;
sem_t C;
sem_t D;
sem_t aux;
sem_t F;



void* funcion_A()
{
    do
    { 
        sem_wait(&A);
        printf("jugando......");
        sem_post(&B);
        //sem_wait(&aux);
    }
    while(cantidad>0);
}

void* funcion_B()
{
    do
    {
        sem_wait(&B);
        printf("gano!\n");
        sem_post(&D);
    
    }
    while(cantidad>0);
    
}

void* funcion_C()
{   
    do
        {

            sem_wait(&B);

            printf("perdio!\n");
            
            sem_post(&D);

        }
    while(cantidad>0);
    
      
    }
 
void* funcion_D()
{
    do
        {
            sem_wait(&D);

            printf("descansa..\n");

            sem_post(&A);
             }

    while(cantidad>0);
}
 
void* funcion_f()
{
    sem_wait(&F);
    printf("fin.\n");
} 
 
 
void main()
{
    unsigned int valor_inicial =1;

    int res = sem_init(&A,0,1);
 
    int res2  = sem_init(&B,0,0);
    int res3  = sem_init(&C,0,0);
    int res4  = sem_init(&D,0,0);
    int res5  = sem_init(&aux,0,0);
    
      int res6  = sem_init(&F,0,0);

   
    pthread_t p1, p2, p3, p4;
 
    int rc;
    rc = pthread_create(&p1,NULL,funcion_A,NULL);
    rc = pthread_create(&p2,NULL,funcion_B,NULL);
    rc = pthread_create(&p3,NULL,funcion_C,NULL);

    rc = pthread_create(&p4,NULL,funcion_D,NULL);


     
    pthread_join(p1,NULL);
    pthread_join(p2,NULL);
    pthread_join(p3,NULL);
    pthread_join(p4,NULL);

    //pthread_join(p5,NULL);


////

     
    sem_destroy(&A);
    sem_destroy(&B);
    sem_destroy(&C);

    sem_destroy(&D);
    sem_destroy(&F);
}

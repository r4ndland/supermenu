#include <stdio.h>
#include <stdlib.h>

//compilar con gcc experimentoPi.c -o ejecutable
//ejecutar con  ./ejecutable

int main(){

  FILE *myFile;
  myFile = fopen("10milDigitosDePi_separados.txt", "r");
  int acum= 0;
  //leer archivo en un array
  int numberArray[10000];
  int i;

  if (myFile == NULL){
    printf("Error en la lectura\n");
    exit (0);
  }

  for (i = 0; i < 10000; i++){
    fscanf(myFile, "%d,", &numberArray[i] );
  }

  printf("Los primeros digitos son\n\n");
  for (i = 0; i < 9999; i++){
    acum=acum+numberArray[i];
  }
  printf("El ultimo numero es: %d\n\n", acum);

  fclose(myFile);

  return 0;
}


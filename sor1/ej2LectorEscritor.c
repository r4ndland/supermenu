#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

pthread_t tid[2];
int counter;
pthread_mutex_t lock;

int material;
int anterior;

void* lector(void *arg)
{
while(1)
{
  pthread_mutex_lock(&lock);
int microseconds; srand (time(NULL));
 
   //generar un numer random entre 1 y 1000:
   microseconds = rand() % 10000 + 1;
 
  


   usleep(microseconds);
  //si la flag esta en true --- lee si no solo espera
   if(material!=anterior) 
    {
        printf("-----------------leyendo %d paginas \n", material);
        anterior=material;
}

  pthread_mutex_unlock(&lock); 
}
}

//semaforo para esperar produccion de datos

void* escribiendo(void *arg)
{
    while( 1 ){

        int microseconds; srand (time(NULL));
 
   //generar un numer random entre 1 y 1000:
   microseconds = rand() % 5000 + 1;

  pthread_mutex_lock( &lock );
  printf( "escribiendo\n" );
  usleep(microseconds);
  microseconds = rand() % 1000 + 1;
  material=microseconds; 
  pthread_mutex_unlock( &lock ); 
}
}

int main(void)
{
    int i = 0;
    int err;

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        return 1;
    }



    pthread_create(&(tid[i]), NULL, &escribiendo, NULL);
    pthread_create(&(tid[i]), NULL, &lector, NULL);

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_mutex_destroy(&lock);

    return 0;
}

#include <stdio.h>
#include <pthread.h>
 
#include <unistd.h>     // para hacer sleep
#include <stdlib.h>     // para libreria de numeros random: srand, rand
#include <time.h>       // para tomar el tiempo
 
#include <semaphore.h>
 
sem_t miSemaforo;
 
 
int variableCompartidaSuma = 0;
//int elementosPitagoricos[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
int apariciones[10] = {0,0,0,0,0,0,0,0,0,0};
int cantidadDePrimos = 0;
pthread_mutex_t lock;
  int numberArray[10000];

FILE *myFile;

#define NUM_THREADS 10
 
void* sumarValor (void* parametro)
{  
   //tomar los datos del thread, imprimirlos, hacer la suma y terminar el thread
   int posArray;
   posArray = (intptr_t) parametro;
   //printf("T = %d \n", posArray);
 
   //sem_wait(&miSemaforo);
   
   int aux = variableCompartidaSuma;
   
   /********************************
   * simular un tiempo para que ocurra una condición de carrera
   *********************************/
   //inicializar la semilla del generador random:
   int microseconds; srand (time(NULL));
 
   //generar un numer random entre 1 y 1000:
   microseconds = rand() % 1000 + 1;
 
   //dormir el thread, simula que esta haciendo alguna tarea


   usleep(microseconds);
   /****************************
   * fin simular un tiempo
   ******************************/
   int aux2 = 0;
   int i;

   int cantidadDePrimosAUX=0;

   int aparicionesAUX[10] = {0,0,0,0,0,0,0,0,0,0};

   int posAux;

int max = posArray+1000; //ese 1000 es la decima parte de 10000
    for(i=posArray;i<max;i++)
{
    aux2 = aux2 + numberArray[i];
//  printf("Suma = %d \n",aux2 );
    //printf("T = %d \n", posArray);

    posAux = numberArray[i];

    aparicionesAUX[posAux]=aparicionesAUX[posAux]+1;

    if(numberArray[i]==2 || numberArray[i]==3 || numberArray[i]==5 || numberArray[i]==7)
    {
    cantidadDePrimosAUX++;
  }
 }      
   
   //aux = aux2 + elementosPitagoricos[posArray]; 

      pthread_mutex_lock(&lock);
   

 
 variableCompartidaSuma+=aux2;
  //printf("var compartida = %d \n", variableCompartidaSuma);
   //sem_post(&miSemaforo);
  cantidadDePrimos = cantidadDePrimos+cantidadDePrimosAUX;

  for(i=0;i<10;i++)
  {
    apariciones[i]+=aparicionesAUX[i];
  }

  pthread_mutex_unlock(&lock);
 
   pthread_exit(NULL);
}
 
int main ()
{

  //poner en funcion
  
  myFile = fopen("10milDigitosDePi_separados.txt", "r");

  //leer archivo en un array

  int i;

  if (myFile == NULL){
    printf("Error en la lectura\n");
    exit (0);
  }

  for (i = 0; i < 10000; i++){
    fscanf(myFile, "%d,", &numberArray[i] );
  }

  //fin abrir archivo

   pthread_t threads[NUM_THREADS]; //una variable de tipo pthread_t sirve para identificar cada hilo que se cree
                                   //la variable threads es una array de pthread_t
                                   //comparar con char data[100], un array de char                                          
   unsigned int value=1;
   int pshared=0;
   int res = sem_init(&miSemaforo, //se inicializa el semaforo pasado por referencia
          pshared,//tipo de semaforo, si pshared=0 el semaforo es local para el proceso actual
                      //en otro caso el semaforo puede ser compartido entre procesos
          value);//valor inicial para el semaforo
 
   if (res != 0) {
    perror("Inicializacion del semaforo ha fallado :(");
    exit(EXIT_FAILURE);
   }
 
 //

    int rc;
    int i1;
    int aux;
    for( i=0; i < NUM_THREADS; i++ ){
     // printf("main() : creando thread %d \n", i);
      aux = i*1000;
      rc = pthread_create(&threads[i],    //identificador unico
                          NULL,        //atributos del thread
                          sumarValor,    //funcion a ejecutar
                          (void *)(intptr_t) aux);    //parametros de la funcion a ejecutar, pasado por referencia
      if (rc){
         printf("Error:unable to create thread, %d \n", rc);
 
         exit(-1);
      }
   }
   
   //join the threads
   for(i = 0 ; i < NUM_THREADS ; i++)
   {
        pthread_join(threads[i] , NULL);
   }
   
   printf("la suma de los primeros 10000 decimales de pi es  : %d \n", variableCompartidaSuma);
   printf("cantidad de numeros primos %d \n",cantidadDePrimos);

   printf("el promedio es %f \n",variableCompartidaSuma/10000.0 );
   int mayor = -1;
   int numero;

  for(i=0;i<10;i++)
  {
    if(apariciones[i]>mayor)
    {
      mayor=apariciones[i];
      numero = apariciones[i];
    }
    //apariciones[i]=aparicionesAUX[i];
    printf("cantidad de %d = %d \n",i,apariciones[i] );

  }
    printf("el numero con mas apariciones es %d = %d \n",numero, apariciones[numero] );

 //printf("El ultimo numero es: %d\n\n", numberArray[9999]);
   //destruir semáforo para no tener conflictos en la próxima ejecución del programa  
   sem_destroy(&miSemaforo);
 
   pthread_exit(NULL);
}
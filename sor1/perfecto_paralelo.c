#include <stdio.h>
#include <pthread.h>
 
#include <unistd.h>     // para hacer sleep
#include <stdlib.h>     // para libreria de numeros random: srand, rand
#include <time.h>       // para tomar el tiempo
 
#include <semaphore.h>
 
sem_t miSemaforo;
 
 
int variableCompartidaSuma = 0;
int elementosPitagoricos[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
 
#define NUM_THREADS 4
 
void* sumarValor (void* parametro)
{  
   //tomar los datos del thread, imprimirlos, hacer la suma y terminar el thread
   int posArray;
   posArray = (intptr_t) parametro;
   printf("T = %d \n", posArray);
 
   sem_wait(&miSemaforo);
   
   int aux = variableCompartidaSuma;
   
   /********************************
   * simular un tiempo para que ocurra una condición de carrera
   *********************************/
   //inicializar la semilla del generador random:
   int microseconds; srand (time(NULL));
 
   //generar un numer random entre 1 y 1000:
   microseconds = rand() % 1000 + 1;
 
   //dormir el thread, simula que esta haciendo alguna tarea
   usleep(microseconds);
   /****************************
   * fin simular un tiempo
   ******************************/
   int aux2 = 0;
   int i;
int max = posArray+4;
    for(i=posArray;i<max;i++)
{
           aux2 = aux2 + elementosPitagoricos[i];
//    printf("Suma = %d \n",aux2 );
 printf("T = %d \n", posArray);
 }      
   
   //aux = aux2 + elementosPitagoricos[posArray];  
   variableCompartidaSuma+=aux2;
 printf("var compartida = %d \n", variableCompartidaSuma);
   sem_post(&miSemaforo);
 
   pthread_exit(NULL);
}
 
int main ()
{
   pthread_t threads[NUM_THREADS]; //una variable de tipo pthread_t sirve para identificar cada hilo que se cree
                                   //la variable threads es una array de pthread_t
                                   //comparar con char data[100], un array de char                                          
   unsigned int value=1;
   int pshared=0;
   int res = sem_init(&miSemaforo, //se inicializa el semaforo pasado por referencia
          pshared,//tipo de semaforo, si pshared=0 el semaforo es local para el proceso actual
                      //en otro caso el semaforo puede ser compartido entre procesos
          value);//valor inicial para el semaforo
 
   if (res != 0) {
    perror("Inicializacion del semaforo ha fallado :(");
    exit(EXIT_FAILURE);
   }
 
 
   int rc;
   int i;
int aux;
   for( i=0; i < NUM_THREADS; i++ ){
      printf("main() : creando thread %d \n", i);
    aux = i*4;
      rc = pthread_create(&threads[i],    //identificador unico
                          NULL,        //atributos del thread
                          sumarValor,    //funcion a ejecutar
                          (void *)(intptr_t) aux);    //parametros de la funcion a ejecutar, pasado por referencia
      if (rc){
         printf("Error:unable to create thread, %d \n", rc);
 
         exit(-1);
      }
   }
   
   //join the threads
   for(i = 0 ; i < NUM_THREADS ; i++)
   {
        pthread_join(threads[i] , NULL);
   }
   
   printf("El numero de la perfeccion es : %d \n", variableCompartidaSuma);
 
   //destruir semáforo para no tener conflictos en la próxima ejecución del programa  
   sem_destroy(&miSemaforo);
 
   pthread_exit(NULL);
}